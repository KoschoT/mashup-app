# ISS Locator
This app can locate the current position of the ISS
and display it on a map. Furthermore the app displays 
the current location as city and country.
If the current location of the ISS is above water, unknown city and 
unknown country are displayed.

### How does it work?
The app requests data from these three APIs

* Google Maps JavaScript API
* Wheretheiss.at API
* geocode.xyz API

Wheretheiss.at API delivers the current latitude and longitude of the
ISS and the app uses the current latitude and longitude to mark the location on
the map and request more data of the location from geocode.xzy.

### How to install the app

Clone the repository and setup git. 

Install all dependencies with `npm install`.

If you only want to start the server once, type `node server`.

For development i would suggest downloading nodemon by typing `npm i -D nodemon`.

After that you can run `npm run dev`. Nodemon will watch your changes and you can

just save your file and reload the page in your browser.
