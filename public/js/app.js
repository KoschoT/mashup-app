//function for loading the location of the ISS
function loadLocation() {
	const http = new XMLHttpRequest();
	const url1 = 'https://api.wheretheiss.at/v1/satellites/25544';
	http.open("GET", url1);
	http.send();
	
	http.onreadystatechange = function() {
		if(this.readyState==4 && this.status==200){
			// saving the response(longitude, latitude) from
			// the geocode API and saving it in variables
			const data = http.response;
			const dataObj = JSON.parse(data);
			const longitude = dataObj.longitude;
			const latitude = dataObj.latitude;
			console.log(longitude);
			console.log(latitude);
	
			//Initiating map with longitude and latitude from Wheretheiss.at
			initMap(latitude, longitude)
	
			const http2 = new XMLHttpRequest();
			//using the latitude and logitude to get data about city and country from geocode
			const url2 = 'https://geocode.xyz/' + latitude + ',' + longitude + '?json=1';
			console.log(url2);
			http2.open("GET",url2);
			http2.send();
					
			http2.onreadystatechange = function() {
				if(this.readyState==4 && this.status==200){
					const data2 = http2.response;
					console.log(data2);
					const dataObj2 = JSON.parse(data2);
					console.log(dataObj2);
					const city = dataObj2.city;
					const country = dataObj2.prov;
					console.log(typeof city);
					console.log(country);
					
					//checking if the current position is over the sea
					if(city === undefined)
					{
						document.getElementById('city').innerHTML = "unknown city";
					}
					else
					{
						document.getElementById('city').innerHTML = city;
					}
	
					if(country === undefined)
					{
						document.getElementById('country').innerHTML = "unknown country";
					}
					else
					{
						document.getElementById('country').innerHTML = country;
					}
				}
			}
		}
	}
}


//Initiating the map with a marker on the position of the ISS
function initMap(lat, lng) {
	var latLong = new google.maps.LatLng(lat, lng);
	console.log(latLong);
	var options = {
		zoom: 3,
		center: latLong
	}
	var map = new google.maps.Map(document.getElementById('map'), options);

	var marker = new google.maps.Marker({
		position: latLong,
		map: map,
		title: 'ISS'
	});

	marker.setMap(map);
}

window.onload = function() {

	loadLocation();

	//filling our accordion with functionality
	var acc = document.getElementsByClassName('accordion');
	var i;
	
	for(i = 0; i < acc.length; i++)
	{
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if(panel.style.display === "block") {
				panel.style.display = "none";
			} else {
				panel.style.display = "block";
			}
		});
	}

	//filling the dark theme switch with functionality	
	const toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');
	
	function switchTheme(e) {
	    if (e.target.checked) {
	        document.documentElement.setAttribute('data-theme', 'dark');
	    }
	    else {
	        document.documentElement.setAttribute('data-theme', 'light');
	    }    
	}
	
	toggleSwitch.addEventListener('change', switchTheme, false);
}

