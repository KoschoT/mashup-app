const express = require('express');
const path = require('path');
var cors = require('cors');

const app = express();

//static folder
app.use(express.static(path.join(__dirname, 'public')), cors());

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log('Server running...'));
